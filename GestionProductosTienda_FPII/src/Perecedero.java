
public class Perecedero extends Producto {
	private int dias;
	public Perecedero(int codigo,String nombre,double precio,int n_unid_disp,int dias){
		super(codigo,nombre,precio,n_unid_disp);
		this.dias=dias;
	}
	public int getDias() {
		return dias;
	}
	public void setDias(int dias) {
		this.dias = dias;
	}
	
	public double precio_reducido(){		
		if(dias==1){
			precio=(precio*0.25);
		}
		else{
			if(dias==2){
				precio=(precio*0.33);
			}else{
				if(dias==3){
					precio=(precio*0.5);
				}else{
					return precio;}
			}
		}
		return precio;
	}
}
