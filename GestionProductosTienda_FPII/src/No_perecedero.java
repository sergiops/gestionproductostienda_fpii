
public class No_perecedero extends Producto {
	Ofertas oferta;
	int n_oferta;
	public No_perecedero(int cod,String nom,double pre,int n_unid_disp,Ofertas oferta){
		super(cod,nom,pre,n_unid_disp);
		this.oferta=oferta;
	}
	public No_perecedero(int cod,String nom,double pre,int n_unid_disp,int n_oferta){
		super(cod,nom,pre,n_unid_disp);
		this.n_oferta=n_oferta;
	}
	
	public Ofertas getOferta() {
		return oferta;
	}
	public void setOferta(Ofertas oferta) {
		this.oferta = oferta;
	}
	public int n_oferta(){
		return n_oferta;
	}
	
}
