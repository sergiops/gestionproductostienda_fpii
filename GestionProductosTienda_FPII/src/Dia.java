
public class Dia extends Empleado{
	private double retencion;
	public Dia(int cod,String nom,String pass,int niv,double produc,double retencion){
		super(cod,nom,pass,niv,produc);
		this.retencion=retencion;
	}
	public double getRetencion() {
		return retencion;
	}
	public void setRetencion(double retencion) {
		this.retencion = retencion;
	}

}
