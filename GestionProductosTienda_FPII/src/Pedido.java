
public class Pedido {
	private Producto[]producto;
	private int longitudpedido;
	private String nombre;
	private double precio;
	private int codigo;
	private int unidades;
	public Pedido(int codigo,String nombre,double precio,int unidades){
		this.codigo=codigo;
		this.nombre=nombre;
		this.precio=precio;
		this.unidades=unidades;
	
	}
	/*public void agregar_al_pedido(){
		longitudpedido++;
	}*/
	public String get_nombre(){
		return nombre;
	}
	public double get_precio(){
		return precio;
	}
	public int get_codigo(){
		return codigo;
	}
	public int get_unidades(){
		return unidades;
	}
	public void set_unidades(int nuevas_unidades){
		unidades=nuevas_unidades;
	}
	
	public int get_codigo_producto(int i){//consultar codigo
		return producto[i].get_codigo();
	}
	public String get_nombre_producto(int i){//consultar nombre
		return producto[i].get_nombre();
	}
	public double get_precio_producto(int i){//consultar precio
		return producto[i].get_precio();
	}
	public int get_unidades_disponibles(int i){//consulta numero unidades disponibles
		return producto[i].get_unid_disponibles();
	}
	public void set_codigo_producto(int i,int codigo_nuevo){	//actualiza codigo
		producto[i].set_codigo(codigo_nuevo);
	}
	public void set_nombre_producto(int i,String nombre_nuevo){	//actualiza nombre
		producto[i].set_nombre(nombre_nuevo);
	}
	public void set_precio_producto(int i,double precio_nuevo){//actualiza precio
		producto[i].set_precio(precio_nuevo);
	}
	public void set_unidades_disponibles(int i,int nuevas_unidades){//actualiza el nuevo de unidades
		producto[i].set_unid_disponibles(nuevas_unidades);
	}
}
	

