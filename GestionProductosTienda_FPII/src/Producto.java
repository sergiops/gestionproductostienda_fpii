
public class Producto implements Cloneable {

	protected int codigo;
	protected String nombre;
	protected double precio;
	protected int n_unidades_disponibles;
	protected String pere;
	protected String nombre2;
	public Producto(int cod,String nom,double pre,int n_unid_disp){
		codigo=cod;
		nombre=nom;
		precio=pre;
		n_unidades_disponibles=n_unid_disp;
	}
	public Producto(String nombre2,String pere){
		this.nombre2=nombre2;
		this.pere=pere;
	}
	
	public Object clone(){
		Object obj=null;
		try{
			obj=super.clone();
		}catch(CloneNotSupportedException ex){
			System.out.println("no se puede duplicar");
		}
		return obj;
	}
	public int get_codigo(){
		return codigo;
	}
	public String get_nombre(){
		return nombre;
	}
	public double get_precio(){
		return precio;
	}
	public int get_unid_disponibles(){
		return n_unidades_disponibles;
	}
	public void set_codigo(int nuevo_codigo){
		codigo=nuevo_codigo;
	}
	public void set_nombre(String nuevo_nombre){
		nombre=nuevo_nombre;
	}
	public void set_precio(double nuevo_precio){
		precio=nuevo_precio;
	}
	public void set_unid_disponibles(int nuevas_unidades){
		n_unidades_disponibles=nuevas_unidades;
	}
	public String get_nombre2(){
		return nombre2;
	}
	public String get_pere(){
		return pere;
	}
}