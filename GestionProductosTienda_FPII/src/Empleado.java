
public class Empleado {

	protected int codigo_acceso;
	protected String nombre_usuario;
	protected String password;
	protected int nivel;
	protected double productividad=0.0;
	public Empleado(int cod,String nom,String pass,int niv,double produc){
		codigo_acceso=cod;
		nombre_usuario=nom;
		password=pass;
		nivel=niv;
		productividad=produc;
	}
	public int get_codigo_acceso(){
		return codigo_acceso;
	}
	public String get_nombre_usuario(){
		return nombre_usuario;
	}
	public String get_password(){
		return password;
	}
	public int get_nivel(){
		return nivel;
	}
	public double get_productividad(){
		return productividad;
	}
	public void set_codigo_acceso(int nuevo_codigo_acceso){
		codigo_acceso=nuevo_codigo_acceso;
	}
	public void set_nombre_usuario(String nuevo_nombre_usuario){
		nombre_usuario=nuevo_nombre_usuario;
	}
	public void set_password(String nuevo_password){
		password=nuevo_password;
	}
	public void set_nivel(int nuevo_nivel){
		nivel=nuevo_nivel;
	}
	public void set_productividad(double nueva_productividad){
		productividad=nueva_productividad;
	}
	public void setPlus_nocturnidad(){
		}
	public void setRetencion(){
		}
	


}