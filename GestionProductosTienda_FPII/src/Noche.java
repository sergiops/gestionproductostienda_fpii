
public class Noche extends Empleado {
	private int plus_nocturnidad;
	public Noche(int cod,String nom,String pass,int niv,double produc,int plus_nocturnidad){
		super(cod,nom,pass,niv,produc);
		this.plus_nocturnidad=plus_nocturnidad;
	}
	public int getPlus_nocturnidad() {
		return plus_nocturnidad;
	}
	public void setPlus_nocturnidad(int plus_nocturnidad) {
		this.plus_nocturnidad = plus_nocturnidad;
	}
}
	