import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Locale;
import java.util.Scanner;


public class Gestion_empleados implements interfaz_niveles { 
	private int numero;
	private Empleado[]empleado;	
	Empleado aux;
	int plus;
	double retencion;
	int codigon;
	String pass;
	int nivel;
	String nombre;
	
	public Gestion_empleados(int numero,Empleado[]empleado){//metodo constructor
		this.numero=numero;
		this.empleado=empleado;
	}	
	public int get_numero_empleado(){//consulta numero de empleado
		return numero;
	}
	public int get_codigo_empleado(int i){//consulta codigo 
		return empleado[i].get_codigo_acceso();
	}
	public String get_nombre_empleado(int i){//consulta nombre
		return empleado[i].get_nombre_usuario();
	}
	public String get_password_empleado(int i){//consulta password
		return empleado[i].get_password();
	}
	public void set_password_empleado(int i,String new_password){//cambia password
		empleado[i].set_password(new_password);	}
		
	
	public Empleado[] cargar_empleados() throws FileNotFoundException{//carga los empleados
		Scanner leer = new Scanner(new File("src/empleados.txt"));//archivo
	    int numeroempleados;
	    leer.useLocale(Locale.US);
	    leer.nextLine();
	    numeroempleados=leer.nextInt();
	    empleado=new Empleado[numeroempleados];
	    leer.nextLine();   
	    for (int i=0;i<numeroempleados;i++) {
	    	leer.nextLine();
	    	nombre=leer.nextLine();
	    	leer.nextLine();
	    	String codigo=leer.nextLine();
	    	codigon=Integer.parseInt(codigo);
	    	leer.nextLine();	    	
	    	pass=leer.nextLine();
	    	leer.nextLine();
	    	String nivell=leer.nextLine();
	    	nivel=Integer.parseInt(nivell);
	    	leer.nextLine();	    	
	    	String turno=leer.nextLine();
	    	if(turno.equals("diurno")){
	    		leer.nextLine();
	    		String retencionn=leer.nextLine();
	    		retencion=Double.parseDouble(retencionn);
	    		 aux=new Dia(codigon,nombre,pass,nivel,0.0,retencion);
	    	}else{
	    		leer.nextLine();
	    		String pluss=leer.nextLine();
	    		plus=Integer.parseInt(pluss);
	    		aux=new Noche(codigon,nombre,pass,nivel,0.0,plus);
	    	}
	    	empleado[i]=aux;
	    }
	    
	    leer.close();//cerramos fichero
	    return empleado;    
	}
	
	public boolean cargar(int codigo_consultar) throws FileNotFoundException{//nos dice si es nocturno o diurno
		Scanner leer = new Scanner(new FileReader ("src/empleados.txt"));//archivo
	    int numeroempleados;
	    leer.useLocale(Locale.US);
	    leer.nextLine();
	    numeroempleados=leer.nextInt();
	    empleado=new Empleado[numeroempleados];
	    leer.nextLine();   
	    for (int i=0;i<numeroempleados;i++) {
	    	leer.nextLine();
	    	String nombre22=leer.nextLine();
	    	leer.nextLine();
	    	String codigo22=leer.nextLine();
	    	int codigon22=Integer.parseInt(codigo22);
	    	leer.nextLine();	    	
	    	String pass22=leer.nextLine();
	    	leer.nextLine();
	    	String nivell22=leer.nextLine();
	    	//int nivel22=Integer.parseInt(nivell22);
	    	leer.nextLine();	    	
	    	String turno22=leer.nextLine();
	    	if(turno22.equals("diurno")&&((codigo_consultar==codigon22))){
	    		leer.nextLine();
	    		String retencionn22=leer.nextLine();	    		
	    		return true;
	    	}else{
	    		leer.nextLine();
	    		String pluss22=leer.nextLine();	    		
	    		return false;
	    	}
	      }
	    
	    leer.close();//cerramos fichero
	    return false;    
	}	  
	
	public int numero_empleados() throws FileNotFoundException{//devuelve el numero de empleados
		Scanner leer = new Scanner(new File("src/empleados.txt"));
	    int numeroempleados;
	    leer.useLocale(Locale.US);
	    leer.nextLine();
	    numeroempleados=leer.nextInt();		
		return numeroempleados;   
	}
	
	public int contraseņa_correcta(int codigo,String password,int numero_empleados) {//comprueba si la contraseņa es correcta
		int valor=0;
		for(int i=0;i<empleado.length;i++){
			if((codigo!=(empleado[i].get_codigo_acceso()))&&(!(password.equals(empleado[i].get_password())))){
				valor=5;
			}
			else {	if((codigo!=(empleado[i].get_codigo_acceso()))){
						valor=111;
						}
					if(!(password.equals(empleado[i].get_password()))){
						valor=222;
						}
			}		
			if((codigo==(empleado[i].get_codigo_acceso()))&&(password.equals(empleado[i].get_password()))){
				valor=-1;
			}
		}	//si el codigo es igual al codigo de acceso buscado en fichero, y la contraseņa introducida es igual a la password del fichero
			//devuelve -1				    
	   return valor;
	}
	
	public void cambiar_contraseņa(int codigo,String pass, int numero_empleados){//para cambiar la contraseņa punto 3
		      for(int i=0;i<numero_empleados;i++){
		    	  if(codigo==(empleado[i].get_codigo_acceso())) {//si el codigo de empleado introducido es igual al codigo de un empleado preciso
		    		  empleado[i].set_password(pass);//se modifica la contraseņa por al nueva
		    		  System.out.println("contraseņa cambiada");
		    	 }		    	  
		  }
	}
	
	public String nombre_empleado(int codigo,int numero_empleados){//devuelde el nombre del empleado
		for(int i=0;i<numero_empleados;i++){
			if(codigo==(empleado[i].get_codigo_acceso())){
				System.out.println("Le atendio: "+empleado[i].get_nombre_usuario());
			}
		}
		return null;		
	}
	public void calculo_productividad(int codigo){	
		int nivel;
		for(int i=0;i<empleado.length;i++){
			if(codigo==empleado[i].get_codigo_acceso()){
				nivel=empleado[i].get_nivel();
				if(nivel==NIVEL1){
					empleado[i].set_productividad(+1);
				}
				if(nivel==NIVEL2){
					empleado[i].set_productividad(+2);
				}
				if(nivel==NIVEL3){
					empleado[i].set_productividad(+3);
				}
			}
		}
		
	}
	public double comprobar_turno(int codigo,double precio_total) throws FileNotFoundException{
		String turno = null;
		double totalll = 0;
		for(int i=0;i<empleado.length;i++){
		if(codigo==empleado[i].get_codigo_acceso()){
			if(cargar(codigo)==true){
				System.out.println("dia");
				turno="dia";
				totalll=diurnos(precio_total,codigo);
				
			}else{
				System.out.println("nocturno");
				turno="noche";	
				Dia diurno;
				diurno=new Dia(codigon,nombre,pass,nivel,0.0,retencion);
				System.out.println("nivel "+diurno.get_nivel());
				nivel=diurno.get_nivel();					
				totalll=plusnocturnidad(i,precio_total,nivel);
			}
		}
		}return totalll;
	}
	public double plusnocturnidad(int i,double precio_total,int nivel){
		double plus_final = 0;							
				if(nivel==NIVEL1){
					if(VALOR_MAX1==200)
					plus_final=2*NIVEL1+(plus/100.0)*precio_total;					
				}
				if(nivel==NIVEL2){					
					double parte1=plus/100.0;
					double parte2=parte1*precio_total;					
					plus_final=NIVEL2+parte2;			
					
				}		
		return plus_final;
		
	}
	public double diurnos(double precio_total,int codigo){			
			int nivel;
			double total = 0;
			for(int i=0;i<empleado.length;i++){
				if(codigo==empleado[i].get_codigo_acceso()){
					nivel=empleado[i].get_nivel();
					if(nivel==NIVEL1){
						total=precio_total+1;
					}
					if(nivel==NIVEL2){
						if(precio_total>VALOR_MAX2)
							total=precio_total+2;
						
					}
					if(nivel==NIVEL3){
						total=3-3*retencion;
					}
				}
			
			
		}return total;
		
	}
	
}
