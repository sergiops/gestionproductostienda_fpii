import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Locale;
import java.util.Scanner;

public class Principal {
	
	public static void main(String[] args) throws Exception {
		Empleado[]empleado = null;//incializo empleado
		Empleado empleado1=null;
		Gestion_empleados empleados=new Gestion_empleados(1,empleado); //creo el objeto "empleados"
		empleados.cargar_empleados();//carga los empleados
		int num_empleados=empleados.numero_empleados();//el metodo numero_empleadoos me dice el numero de empleados que hay en el fichero
		
		Producto[]producto=null;//inicializo producto
		Gestion_ventas productos=new Gestion_ventas(producto);//creo el objeto "productos"
		int num_productos=productos.numero_productos();//el metodo numero_productos me dice el numero de productos que hay en el fichero
		productos.cargar_productos();
		Pedido[]pedido=null;
	   	Scanner leer=new Scanner(System.in);//para leer con escaner por teclado
		int codigo=0;//inicializo variables
		String password;
		int valor=0;	
		int opcion;
		int opcion1 = 0;
		int opcion2;
		int i=0;
		int productos_comprar;
		//int prod_elegido;
		//int unidades;
		double productividad=0.0;
		
		do{
			do{	
				try{
				System.out.printf("introduce el login");			//la comprobaci�n usuario y contrase�a
				codigo=leer.nextInt();//leo el codigo
				leer.nextLine();
				System.out.printf("introduce la contrase�a");			
				password=leer.nextLine();	//leo el password				
				valor=empleados.contrase�a_correcta(codigo,password,num_empleados);	
				if(valor==5)throw new Excepciones();
				else{if(valor==111)throw new Excepciones();
					if(valor==222)throw new Excepciones();
				}
				}
				catch(Excepciones a){
					Excepciones e1=new Excepciones();
					System.out.println(e1.get_codigo(valor));					
				}
				
			}while(valor!=-1);	//mientas que sea distinto de -1, es decir que el login y la contrase�a sean distintos de los del fichero, que siga en el bucle
				
				do{	
					menuprincipal();
					opcion=leer.nextInt();
					switch(opcion){
						case 1:
							//hacer pedido
							do{									
								System.out.println("�Cuantos tipos de productos deseas comprar?");
								productos_comprar=leer.nextInt();//introduzco los productos que voy a comprar
							}while(productos_comprar>num_productos);
								 
							 do{			  
								 menu1();
								opcion1=leer.nextInt();
								
								switch(opcion1){
									
										case 1:				
											productos.contadorelegidos(productos_comprar);
											if(productos.hacerpedido(productos_comprar)!=null){//mientas que no devuelva 0, quiero decir que se estan a�adiendo productos al pedido
												System.out.println("\nPedido terminado\n");														
											}	
																			
										break;
										case 2:
											//precio de los productos										
											System.out.println("El total es: "+productos.calculo_pedido());//la suma del precio
											
										break;
										case 3://imprime la factura
											System.out.println("Factura:\n");
											productos.visualiza_factura(productos.calculo_pedido());//metodo visualiza_factura para que me diga el nombre,codigo y precio de los productos del pedido
											empleados.nombre_empleado(codigo, num_empleados);//me vuelve el nombre del empleado, llamando al metodo nombre_empleado
											productividad=empleados.comprobar_turno(codigo,productos.calculo_pedido());												
											
										break;
										case 4:												
										break;	
										}
								
							 	}while(opcion1!=4);//mientras que la opcion productos_comprar sea mayor a 0 O opcion sea distinto de 4, el programa sigue en el bucle							
						break;
						case 2:
							productos.mostrar_productos(num_productos);
							leer.nextLine();
							System.out.printf("el numero del producto deseas modificar 1,2?");
							i=leer.nextInt();
							i--;
								menu2();
								opcion2=leer.nextInt();	
								
								switch(opcion2){
									case 01://cambia el nombre
										String nuevo_nombre;
										do{
										System.out.println("introduce el nuevo nombre\n");
										leer.nextLine();
										nuevo_nombre=leer.nextLine();
										}while(productos.modificar_nombre(i,nuevo_nombre,num_productos)!=0);//metodo modificar_nombre y si es distinto de 0 sigue en el bucle
										System.out.println("nombre modificado");//cuando sea 0 entonces es que se ha modificado el nombre
											
										
										break;
									case 02:	//cambia el precio
										System.out.println("Introduce el nuevo precio");
										double nuevo_precio=leer.nextDouble();//leo el nuevo precio
										productos.modificar_precio(i,nuevo_precio);//metodo modificar_precio
										
										break;
									case 03://cambia el codigo
										int nuevo_codigo;
										do{
											System.out.println("Introduce el nuevo codigo");
											nuevo_codigo=leer.nextInt();//introduce el nuevo_codigo
										}while(productos.modificar_codigo(i,nuevo_codigo, num_productos)!=0);//llamo al metodo modificar_codigo
										System.out.println("codigo modificado");//cuando sea 0 entonces es que se ha modificado el nombre
										break;
									case 04://cambia el numero de unidades
										int nuevas_unidades;
										
										System.out.println("numero de unidades nuevas");
										nuevas_unidades=leer.nextInt();								
										++i;
										productos.cambiar_unidades_disponibles(i,nuevas_unidades);
																				
									}
							
						break;
						case 3://cambia contrase�a del empleado
								leer.nextLine();
								System.out.printf("introduzca contrase�a\n");
								String contrase�a_nueva=leer.nextLine();//leemos la contrase�a			
								empleados.cambiar_contrase�a(codigo,contrase�a_nueva,num_empleados);
								//llamamos al metodo cambiar_contrase�a
															
							break;
						case 4:
							//desconectar el sistema y volver a solicitar usuario y contrase�a
							
							break;			
					}					

		}while(opcion!=4);	//el programa no para de funcionar	
				System.out.println("productividad:"+productividad);
		}while(opcion==4);//si la opcion es igual a 4 sale del do-while y vuelve al inicio
	}
	public static void menuprincipal(){
		System.out.printf("Menu principal\n");
		System.out.printf("	1.Hacer pedido\n");
		System.out.printf("	2.Modificar producto\n");
		System.out.printf("	3.Cambiar contrase�a empleado\n");
		System.out.printf("	4.Log out\n");
	}
	public static void menu1(){
		System.out.printf("	1.1 A�adir un tipo de producto\n");
		System.out.printf("	1.2 Visualizar precio total\n");
		System.out.printf("	1.3 Imprimir factura\n");
		System.out.printf("	1.4 Terminar pedido\n");
	}
	public static void menu2(){
		System.out.printf("	1.Modificar nombre\n");
		System.out.printf("	2.Modificar precio\n");
		System.out.printf("	3.Modificar c�digo\n");
		System.out.printf("	4.Modificar n�mero de unidades");
	}
}


