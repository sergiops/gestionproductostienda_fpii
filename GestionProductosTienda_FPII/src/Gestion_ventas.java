import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;


public class Gestion_ventas {
	private Producto[]producto;
	Producto aux;
	private Pedido[]pedido=null;
	Ofertas[] oferta;
	Ofertas aux1;
	String perecedero;
	int j=0;
	int k=0;
	int i=0;
	int cantidad=0;
	boolean repetido=false;
	Producto punto1;
	Producto punto2;
	int unidades;
	int codigon;
	double precion;
	int unidades1;
	int dias;
	int n_oferta;
	String nombre11;
	String nombre_teclado=null;
	int unidadesp;
	String nombre_fichero;
	int v=0;
	Scanner leer1=new Scanner(System.in);
	No_perecedero precio = null;
	Producto[]comprueba = null;
	double total_suma;
	int oferta_numero_si;
	
	public Gestion_ventas(Producto[]producto){	//constuctor
		this.producto=producto;
	}
	public Gestion_ventas(){}
	
	public int get_codigo_producto(int i){//consultar codigo
		return producto[i-1].get_codigo();
	}
	public String get_nombre_producto(int i){//consultar nombre
		return producto[i-1].get_nombre();
	}
	public double get_precio_producto(int i){//consultar precio
		return producto[i-1].get_precio();
	}
	public int get_unidades_disponibles(int i){//consulta numero unidades disponibles
		return producto[i-1].get_unid_disponibles();
	}
	public void set_codigo_producto(int i,int codigo_nuevo){	//actualiza codigo
		producto[i-1].set_codigo(codigo_nuevo);
	}
	public void set_nombre_producto(int i,String nombre_nuevo){	//actualiza nombre
		producto[i-1].set_nombre(nombre_nuevo);
	}
	public void set_precio_producto(int i,double precio_nuevo){//actualiza precio
		producto[i-1].set_precio(precio_nuevo);
	}
	public int cambiar_unidades_disponibles(int i,int nueva_cantidad){//actualiza el nuevo de unidades
		int suma_unidades;
			while(nueva_cantidad<=0){
				System.out.println("Error.Por favor vuelva a introducir la cantidad");//error y mensaje al empleado
			}
			suma_unidades=producto[i-1].get_unid_disponibles()+nueva_cantidad;
			producto[i-1].set_unid_disponibles(suma_unidades);
			return suma_unidades;	//seran las unidades disponibles + nueva_cantidad
		
	}
	
	public Producto[] cargar_productos() throws FileNotFoundException{//carga los productos
		Scanner leer = new Scanner(new FileReader ("src/productos.txt"));//archivo
	    int numeroproductos;
	    leer.useLocale(Locale.US);
	    leer.nextLine();//leo la primera linea
	    numeroproductos=leer.nextInt();//lee los numeros de productos
	    producto=new Producto[numeroproductos];//meto a producto el nuevo de productos
	    leer.nextLine();	   
	    for (int i=0;i<numeroproductos;i++) {//mientras el numero de procutos sea mayor que i, i++
	    	leer.nextLine();
	    	String codigo=leer.nextLine();//se lee el codigo
	    	codigon=Integer.parseInt(codigo);//se convierte de string en entero
	    	leer.nextLine();
	    	nombre11=leer.nextLine();//se lee el nombre
	    	leer.nextLine();
	    	String precio=leer.nextLine();//se lee el precio
	    	precion=Double.parseDouble(precio);//se convierte el precio de String a double	    	
	    	leer.nextLine();
	    	String unidad=leer.nextLine();
	    	unidades1=Integer.parseInt(unidad);//se convierte de string en entero
	    	leer.nextLine();	    	
	    	perecedero=leer.nextLine();
	    	if(perecedero.equalsIgnoreCase("si")){
	    		leer.nextLine();
		    	String dia=leer.nextLine();
		    	dias=Integer.parseInt(dia);
		    	aux=new Perecedero(codigon,nombre11,precion,unidades1,dias);
	    	}else{if(perecedero.equalsIgnoreCase("no")){
	    		leer.nextLine();
		    	String oferta=leer.nextLine();
		    	n_oferta=Integer.parseInt(oferta);
		    	aux=new No_perecedero(codigon,nombre11,precion,unidades1,n_oferta);
	    	}
	    	}	
	    	producto[j++]=aux;
	     }	
	    leer.close();//se cierra el archivo
		return producto;
		}
	
	public boolean cargar(String nombre_consultar) throws FileNotFoundException{//carga los productos
			Scanner leer = new Scanner(new FileReader ("src/productos.txt"));//archivo
		    int numeroproductos;
		    leer.useLocale(Locale.US);
		    leer.nextLine();//leo la primera linea
		    numeroproductos=leer.nextInt();//lee los numeros de productos
		    leer.nextLine();	   
		    for (int i=0;i<numeroproductos;i++) {//mientras el numero de procutos sea mayor que i, i++
		    	leer.nextLine();
		    	String codigo=leer.nextLine();//se lee el codigo
		    	int codigon2=Integer.parseInt(codigo);//se convierte de string en entero
		    	leer.nextLine();
		    	String nombre112=leer.nextLine();//se lee el nombre		    	
		    	leer.nextLine();
		    	String precio=leer.nextLine();//se lee el precio
		    	double precion2=Double.parseDouble(precio);//se convierte el precio de String a double	    	
		    	leer.nextLine();
		    	String unidad=leer.nextLine();
		    	int unidades12=Integer.parseInt(unidad);//se convierte de string en entero
		    	leer.nextLine();	    	
		    	String perecedero2=leer.nextLine();
		    	if(perecedero2.equalsIgnoreCase("si")&&(nombre_consultar.equalsIgnoreCase(nombre112))){
		    		leer.nextLine();
			    	String dia=leer.nextLine();
			    	int dias2=Integer.parseInt(dia);							
						return true;
		    	}else{
		    		leer.nextLine();
			    	String oferta=leer.nextLine();
			    	int n_oferta2=Integer.parseInt(oferta);
			    	
			    	return false;
		    	
		    	}		     
		    }
		    leer.close();//se cierra el archivo
			return false;
			}
	
	public int numero_productos() throws FileNotFoundException{//para saber el numero de productos que existen en el fichero
		Scanner leer = new Scanner(new FileReader ("src/productos.txt"));
	    int numeroproductos;
	    leer.useLocale(Locale.US);
	    leer.nextLine();
	    numeroproductos=leer.nextInt();
		leer.close();
		return numeroproductos;   
	}
	public int modificar_nombre(int num_prod,String nuevo_nombre,int cantidad_productos) throws IOException{
		int i=0;//num_prod es el numero de producto elegido, la cantidad_productos es el numero de productos que existe
				do{
					if(producto[i].get_nombre().equals(nuevo_nombre)){//comparo si los nombres de los productos son igual al nuevo nombre
						System.out.println("Error nombre existente\n");
						return 1;}
						i++;			//si no se hace el if, se incrementa el i++		
				}while(i<cantidad_productos);	//todo esto se hace mientras que i sea menor k la cantidad de productos						
			    producto[num_prod].set_nombre(nuevo_nombre);//al producto elegido se le modifica el nombre
	    		return 0;
				
	}
	
	public void modificar_precio(int num_prod,double nuevo_precio){
		producto[num_prod].set_precio(nuevo_precio);    //al producto elegido se le modifica el precio
		System.out.println("Precio modificado\n");
	}
	
	public int modificar_codigo(int num_prod,int nuevo_codigo,int cantidad_productos){
		int i=0;
		do{
			if(producto[i].get_codigo()==(nuevo_codigo)){//hacemos lo mismo que en el caso del nombre, pero ahora en vez de equals se pone == para numeros
			System.out.println("Error, nombre existente\n");
			return 1;}
			i++;
		}while(i<cantidad_productos);	
		producto[num_prod].set_codigo(nuevo_codigo);
		return 0;
}	
	public int modificar_cantidad(int nueva_cantidad,int disponibles){
		int suma_unidades;
		while(nueva_cantidad<=0){
			System.out.println("Error.Por favor vuelva a introducir la cantidad");//error y mensaje al empleado
		}
		suma_unidades=disponibles+nueva_cantidad;
		return suma_unidades;	//seran las unidades disponibles + nueva_cantidad
	}
	
	public void contadorelegidos(int tamano){
		 pedido=new Pedido[tamano];
	 }	
	public int consultar_producto(String nombre2){//mira en productos
		for(int k=0;k<producto.length;k++){			
				if(nombre2.equalsIgnoreCase(producto[k].get_nombre())){
					return k;
				}
			}return -1;		
	}

	public Pedido[] hacerpedido(int tipos_de_productos) throws Exception {		
		int unidades_disponibles = 0;
		int num_prod=numero_productos();				
		mostrar_productos(num_prod);//cargar producto, se muestran y se crea el objeto	
		
		for(int i=0;i<tipos_de_productos;){
			System.out.println("Elige el nombre del producto para a�adir al pedido:\n");
			nombre_teclado=leer1.next();
			if(consultar_producto(nombre_teclado)!=-1){
				k=consultar_producto(nombre_teclado);
				unidades_disponibles=producto[k].get_unid_disponibles();
				nombre_fichero=producto[k].get_nombre();
				try{
					System.out.println("Cantidad del producto");
					cantidad=leer1.nextInt();
					if(cantidad>unidades_disponibles)throw new Excepciones();
				}
				catch(Excepciones e){
					System.out.println(e.get_codigo(333));//muyyyyyyy biennnnnn
					return null;
				}
				if(i==0){
					j=0;
					pedido=construir_pedido(j,k);	
					System.out.println("A�adido a pedido");
					i++;
					j++;
				}else{							
						j--;
						if(consultar_pedido(nombre_teclado,i)){
							System.out.println("Error, producto a�adido");
						}else{							
							pedido=construir_pedido(j,k);	
							System.out.println("A�adido a pedido");
							i++;
							j++;
							}
				}
					
			}//fin del if
		
		}//fin del for
		System.out.println("ya no puede a�adir m�s productos\n");
		return pedido;
	}
	public boolean consultar_pedido(String nombre,int tope){//k producto, y la j pedido
		repetido=false;				
		for(j=0;j<tope;j++){
				if(pedido[j].get_nombre()==null){
				}else{if(nombre.equalsIgnoreCase(pedido[j].get_nombre())){//miramos dentro de pedido	
					repetido=true;
						repetido=true;
					return repetido;			
					}//fin del if
				}
			
		}return repetido;
	}
	public Pedido[] construir_pedido(int j,int k){
		
		pedido[j]=new Pedido(producto[k].get_codigo(),producto[k].get_nombre(),producto[k].get_precio(),producto[k].get_unid_disponibles());		
				
			Producto punto1=new Producto(producto[k].get_codigo(),producto[k].get_nombre(),producto[k].get_precio(),producto[k].get_unid_disponibles());
			Producto punto2=(Producto)punto1.clone();
			unidadesp=punto1.get_unid_disponibles();
			
			pedido[j].set_unidades(cantidad);	
			producto[k].set_unid_disponibles(unidadesp-cantidad);
			
			return pedido;
	}
	
	public Ofertas[] leer_ofertas() throws FileNotFoundException{	  
		Scanner leer1 = new Scanner(new FileReader ("src/ofertas.txt"));//archivo
	    int numero_ofertas;
	    leer1.useLocale(Locale.US);
	    leer1.nextLine();//leo la primera linea
	    numero_ofertas=leer1.nextInt();//lee los numeros de productos
	    oferta=new Ofertas[numero_ofertas];//meto a producto el nuevo de productos
	    leer1.nextLine();	
	    Ofertas[] oferta=new Ofertas[3];			
	    for (int i=0;i<numero_ofertas;i++) {//mientras el numero de procutos sea mayor que i, i++
	    	leer1.nextLine();
	    	String id=leer1.nextLine();//se lee el ID Oferta
	    	int id_oferta=Integer.parseInt(id);//se convierte de string en entero
	    	leer1.nextLine();
	    	String tipo=leer1.nextLine();//se lee el tipo	    	
			if(tipo.equals("2x1")){
	    		oferta[0]=new O_2x1(id_oferta);	    		
	    	}else{
	    		if(tipo.equals("3x2")){
	    			oferta[1]=new O_3x2(id_oferta);
	    		}
	    		else{
	    			if(tipo.equalsIgnoreCase("porcentaje")){
	    				leer1.nextLine();
	    				String tantopor=leer1.nextLine();//se lee el %
	    				int porcentaje=Integer.parseInt(tantopor);
	    				leer1.nextLine();
	    				String max=leer1.nextLine();
	    		    	int maximo=Integer.parseInt(max);//se convierte de string en entero
	    		    	oferta[2]=new O_descuento(id_oferta,porcentaje,maximo);
	    			}
	    		}
	    	} 	    	
	     }	
	   	    
	    leer1.close();//se cierra el archivo	
	    return oferta;
	    }
	
	public double calculo_pedido() throws FileNotFoundException /*throws FileNotFoundException*/{//calcular el precio de pedido
		double suma=0.0;
		double suma1=0.0;		
		for(int i=0;i<pedido.length;i++){
			if((cargar(pedido[i].get_nombre())==true)){
				Perecedero pere;
					pere = new Perecedero(pedido[i].get_codigo(),pedido[i].get_nombre(),pedido[i].get_precio(),pedido[i].get_unidades(),dias);
					suma=pere.precio_reducido()*pedido[i].get_unidades();
					
			}
			else{
				oferta=leer_ofertas();
				if((cargar(pedido[i].get_nombre())==false)){
					for(int j=0;j<producto.length;j++){
						No_perecedero no_pere;
						if(nombre11.equalsIgnoreCase(pedido[i].get_nombre())){
							no_pere = new No_perecedero(pedido[i].get_codigo(),pedido[i].get_nombre(),pedido[i].get_precio(),pedido[i].get_unidades(),n_oferta);							
							if(n_oferta==11){
								suma1=oferta[0].oferta(pedido[i].get_precio(), pedido[i].get_unidades());
							}else{
								if(n_oferta==13){
									suma1=oferta[1].oferta(pedido[i].get_precio(), pedido[i].get_unidades());
								}
								else{
									suma1=oferta[2].oferta(pedido[i].get_precio(), pedido[i].get_unidades());
							}}
						
						}
					}
				}
			}
			
		}
		double total_suma=suma+suma1;
		return total_suma;//devuelve suma
	}
	public void visualiza_factura(double total) throws FileNotFoundException{		//para ver la factura con los productos el nombre, codigo y precio
		for(int i=0;i<pedido.length;i++){
			System.out.println("Producto N�="+(i+1));
			System.out.println("nombre: "+pedido[i].get_nombre());
			System.out.println("codigo: "+pedido[i].get_codigo());
			System.out.println("Unidades:"+pedido[i].get_unidades()+"\n");			
		}	
		
		System.out.println("Precio total: "+total);
	}
	public void mostrar_productos(int num_prod){
		for(int i=0;i<num_prod;i++){
			System.out.println("Codigo: "+producto[i].get_codigo());
			System.out.println("Nombre: "+producto[i].get_nombre());
			System.out.println("Precio: "+producto[i].get_precio());
			System.out.println("Unidades: "+producto[i].get_unid_disponibles());
		}
	}
	
}
