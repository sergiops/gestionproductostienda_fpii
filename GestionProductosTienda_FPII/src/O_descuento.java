public class O_descuento extends Ofertas {
	private int tantoporciento;	
	private int maxunidades;
	
	public O_descuento(int ID,int tantoporciento,int maxunidades){
		super(ID);
		this.tantoporciento=tantoporciento;
		this.maxunidades=maxunidades;
	}

	public double oferta(double precio, int cantidad_comprar){//asi seria mejor xk coge parametros constructor
		double descuento;
		int cantidad_sin_descuento;
		if(cantidad_comprar>maxunidades){			
			cantidad_sin_descuento=cantidad_comprar-maxunidades;
			descuento=maxunidades*precio*(100-tantoporciento)/100;
			descuento+=cantidad_sin_descuento*precio;
		}else{descuento=maxunidades*precio*(100-tantoporciento)/100;
		}
		return (precio*cantidad_comprar)-descuento;
		
	}
	
}