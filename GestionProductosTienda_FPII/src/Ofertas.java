
public abstract class Ofertas {
	protected int ID;
	public Ofertas(int ID){
		this.ID=ID;
	}
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public abstract double oferta(double precio,int unidades);
	
}
